import cv2 as cv
import numpy as np
from threading import Lock
from spool import Spool
from time import sleep

class Detection(Spool):

    COG_SIZE = 28
    BOBBER_SIZE = 30
    MSE_THRESHOLD = 5800    # higher number is more differenter
    # threading properties
    stopped = True
    lock = None
    rectangles = []
    #properties
    cascade = None
    screenshot = None
    cog_tooltip = None
    cog_confirmed = False
    mouse_position = None
    screenshot_dimensions = None
    x_mon_offset = 0
    hist_threshold = 0
    fish_hooked = False
    

    def __init__(self, model_file_path, cog_file_path, x_mon_offset, hist_threshold):
        # create thread lock object
        self.lock = Lock()
        self.x_offset = x_mon_offset

        # load the trained cascade model
        self.cascade = cv.CascadeClassifier(model_file_path)
        # load our cog template and convert so we can compare histograms to mouse cursor
        self.hist_threshold = hist_threshold
        self.cog_tooltip = cv.imread(cog_file_path, cv.IMREAD_UNCHANGED)
        self.cvt_cog_tooltip = cv.cvtColor(self.cog_tooltip, cv.COLOR_BGR2HSV)
        
 
    # information from main thread we need to do our job
    def update(self, screenshot, mouse_pos):
        self.lock.acquire()
        self.screenshot = screenshot
        self.mouse_position = mouse_pos
        self.lock.release()


    # information we output to main thread object
    def run(self):
        fish_hooked = False
        while not self.stopped:
            if not self.screenshot is None:
                # do object detection
                rectangles = self.cascade.detectMultiScale(self.screenshot)
                cog_confirmed = self.confirm_tooltip_hist()
                if self.cog_confirmed:
                    #cv.imwrite('debug_images/bobber-debug.png',(self.get_bobber_image()))
                    fish_hooked = self.confirm_bobber_splash()
                # lock the thread while updating the results
                self.lock.acquire()
                self.cog_confirmed = cog_confirmed
                self.rectangles = rectangles
                self.fish_hooked = fish_hooked
                self.lock.release()


    def get_cursor_image(self):
        # Will return the exact size of cog if its the active cursor
        # Mouse position returns x,y across entire screenspace so,
        # we use the offset to normalize it to the x,y on screenshot image
        x = self.mouse_position[0]
        y = self.mouse_position[1]
        # numpy slice to crop the image
        # https://stackoverflow.com/a/58177717/17825978
        # image[y1:y2, x1:x2]
        y1 = y
        y2 = y + self.COG_SIZE
        x1 = x - self.x_offset
        x2 = (x - self.x_offset) + self.COG_SIZE
        return self.screenshot[y1:y2, x1:x2]


    def confirm_tooltip_hist(self):
        # Compare histogram of cog template vs current mouse cursor image
        cursor_img = self.get_cursor_image()
        #str_pos = str(self.mouse_position[0]) + "-" + str(self.mouse_position[1])
        #cv.imwrite('debug_images/{}.png'.format(str_pos),cursor_img)
        # https://docs.opencv.org/3.4/d8/dc8/tutorial_histogram_comparison.html
        cvt_cursor_img = cv.cvtColor(cursor_img, cv.COLOR_BGR2HSV)
        h_bins = 50
        s_bins = 60
        histSize = [h_bins, s_bins]
        h_ranges = [0, 180]
        s_ranges = [0, 256]
        ranges = h_ranges + s_ranges
        channels = [0, 1]
        hist_cursor_img = cv.calcHist([cvt_cursor_img], channels, None, histSize, ranges, accumulate=False)
        cv.normalize(hist_cursor_img, hist_cursor_img, alpha=0, beta=1, norm_type=cv.NORM_MINMAX)
        hist_cog_tooltip = cv.calcHist([self.cvt_cog_tooltip], channels, None, histSize, ranges, accumulate=False)
        cv.normalize(hist_cog_tooltip, hist_cog_tooltip, alpha=0, beta=1, norm_type=cv.NORM_MINMAX)
        intersection = 2
        cursor_cog_tooltip = cv.compareHist(hist_cursor_img, hist_cog_tooltip, intersection)
        if (cursor_cog_tooltip > self.hist_threshold):
            return True
        return False

        #for compare_method in range(4):
        #    cursor_cog_mask = cv.compareHist(hist_cursor_img, hist_cog_mask, compare_method)
        #    cursor_cog_tooltip = cv.compareHist(hist_cursor_img, hist_cog_tooltip, compare_method)
        #    cog_on_cog = cv.compareHist(hist_cog_mask, hist_cog_tooltip, compare_method)
        #    print('Method:', compare_method, 'Cursor_mask, cursor_tooltip, cog_cog :',\
        #        cursor_cog_mask, '/', cursor_cog_tooltip, '/', cog_on_cog)    
        #self.stop()


    def get_bobber_image(self):
        # Take a bigger slice of the screenshot for bobber
        x = self.mouse_position[0]
        y = self.mouse_position[1]
        y1 = y - self.BOBBER_SIZE
        y2 = y + self.BOBBER_SIZE
        x1 = (x - self.x_offset) - self.BOBBER_SIZE
        x2 = (x - self.x_offset) + self.BOBBER_SIZE
        return self.screenshot[y1:y2, x1:x2]


    def confirm_bobber_splash(self):
        # Compare our bobber image every half second
        step = self.get_bobber_image()
        sleep(.25)
        half_step = self.get_bobber_image()
        mse = self.mean_square_error(step, half_step)
        if mse > self.MSE_THRESHOLD:
            print(mse)
            return True
        return False


    def mean_square_error(self, img1, img2):
        # Credit: https://github.com/Lime365/wow-fishbot
        # Honestly, dont understand but testing of bobber images proved worthy
        # Intuitively seems like the best way to compare because math 
        mse = np.sum((img1.astype("float") - img2.astype("float")) ** 2)
        mse /= float(img1.shape[0] * img1.shape[1])
        return mse 