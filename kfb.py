from time import sleep, time
from threading import Lock
from spool import Spool
from math import sqrt
from random import uniform


class BotState:
    INITIALIZING = 0
    CASTING = 1
    SEARCHING = 2
    FISHING = 3


class KingFisherBot(Spool):

    # constants
    INITIALIZING_SECONDS = 10
    MAX_FISHING_SECONDS = 30
    TOOLTIP_MATCH_THRESHOLD = 0.99
    SEARCH_ATTEMPTS_THRESHOLD = 10
    #threading properties
    stopped = True
    lock = None
    #properties
    state = None
    targets = []
    screenshot = None
    timestamp = None
    x_offset = 0
    mon_w = 0
    mon_h = 0
    cog_tooltip = None
    cog_mask = None
    last_cast_time = None
    first_cast = True
    cvt_cog_tooltip = None
    cvt_cog_mask = None
    fish_hooked = False

    def __init__(self, joystick, x_offset, monitor_size):

        self.lock = Lock()
        self.joystick = joystick
        self.x_offset = x_offset


        # start kfb in the initializing mode to allow
        # mark the time at which this started 
        self.state = BotState.INITIALIZING
        self.timestamp = time()

    # threading methods
    # information we receive from main thread to do our job
    def update_targets(self, targets):
        self.lock.acquire()
        self.targets = targets
        self.lock.release()


    def update_screenshot(self, screenshot):
        self.lock.acquire()
        self.screenshot = screenshot
        self.lock.release()


    def update_cog_status(self, cog_status):
        self.lock.acquire()
        self.cog_status = cog_status
        self.lock.release()


    def update_bobber_status(self, bobber_status):
        self.lock.acquire()
        self.fish_hooked = bobber_status
        self.lock.release()


    # information we output to main thread 
    def run(self):
        while not self.stopped:
            if self.state == BotState.INITIALIZING:
                # do not do bot actions until the startup waiting period is complete
                if time() > self.timestamp + self.INITIALIZING_SECONDS:
                    # start casting when the waiting period is over
                    self.joystick.stand()
                    self.lock.acquire()
                    self.state = BotState.CASTING
                    self.lock.release()

            elif self.state == BotState.CASTING:
                print("Casting")
                if self.first_cast:
                    self.last_cast_time = time()
                    self.joystick.cast()
                    self.first_cast = False
                    self.lock.acquire()
                    self.state = BotState.SEARCHING
                    self.lock.release()
                # only cast maximum every 10 seconds
                elif (time() - self.last_cast_time) > self.SEARCH_ATTEMPTS_THRESHOLD:
                    self.last_cast_time = time()
                    self.joystick.cast()
                    self.lock.acquire()
                    self.state = BotState.SEARCHING
                    self.lock.release()
                else:
                    self.lock.acquire()
                    self.state = BotState.SEARCHING
                    self.lock.release()

            elif self.state == BotState.SEARCHING:
                # Always search for bobber for at least 10 seconds before casting again
                # Wait for a ROI from detector to be passed in
                if len(self.targets) < 1:
                    continue
                else:
                    print("Searching")
                    # Hover target will search through a list of targets looking for cog  
                    found_cog = self.hover_targets()
                    if found_cog:
                        self.lock.acquire()
                        self.state = BotState.FISHING
                        self.lock.release()
                    elif ((time() - self.last_cast_time) < self.SEARCH_ATTEMPTS_THRESHOLD):
                        continue
                    else:
                        self.lock.acquire()
                        self.state = BotState.CASTING
                        self.lock.release()

            elif self.state == BotState.FISHING:
                # Only hover for ~30 seconds from cast time, considering it a fail if we timeout
                if (time() - self.last_cast_time + uniform(-2,2)) > self.MAX_FISHING_SECONDS:
                    sleep(.5)
                    self.joystick.shift_right_click()
                    sleep(1)
                    print("Timed out after ~30 seconds")
                    self.lock.acquire()
                    self.state = BotState.CASTING
                    self.lock.release()
                if self.fish_hooked:
                    # GO TIME
                    print("BONANZA!")
                    sleep(.5)
                    self.joystick.shift_right_click()
                    sleep(1)
                    self.lock.acquire()
                    self.state = BotState.CASTING
                    self.lock.release()


    # Bot action methods
    def hover_targets(self):
        # 1. Order targets by distance (in case someone else is fishing or the model doesnt account for everything)
        # loop:
        #   2. hover over the nearest target
        #   3. Confirm that its our bobber via cog
        #   4. IF its not check the next target
        # endloop
        # 5. IF no target was found return false
        # 6. else return true and set state fishing
        print('Targets: {}'.format(str(len(self.targets))))
        targets = self.targets_ordered_by_distance(self.targets)
        target_index = 0
        found_bobber = False
        
        while not found_bobber and target_index < len(targets):
            # if we stopped our script exit this loop
            if self.stopped:
                break

            # load up next target in the list and convert those coords
            target_pos = targets[target_index]
            # Since we get the x,y from image, we add offset of our entire screen x,y for the mouse
            t_x = target_pos[0] + self.x_offset
            t_y = target_pos[1]
            print('Moving mouse to x:{} y:{}'.format(t_x, t_y))
            sleep(.25)
            # move the mouse
            self.joystick.move_mouse(x=t_x, y=t_y)
            # Cog should appear right away, but just wait a split second to be sure
            sleep(.25)
            # confirm cog
            if self.cog_status:
                print('Confirmed cog at at x:{} y:{}'.format(t_x, t_y))
                found_bobber = True
            else:
                print("cog not detected?!")
            target_index += 1
        return found_bobber


    def targets_ordered_by_distance(self, targets):
        center_pos = (self.mon_w / 2, self.mon_h / 2)
        # sqrt((x1 - x2)^2 + (y2 - y1)^2)
        def pythagorean_distance(pos):
            return sqrt((pos[0] - center_pos[0])**2 + (pos[1] - center_pos[1])**2)
        targets.sort(key=pythagorean_distance)
        return targets


    def confirm_fish(self):
        print("hello world")

            
