import cv2 as cv
from time import time
from monitorcapture import MonitorCapture
from vision import Vision
from control import Controller
from detection import Detection
from kfb import KingFisherBot, BotState

# ======= Config =======
DEBUG_DETECT    = True
DEBUG_FPS       = False
MONITOR         = 1         # Monitor 0 is all monitors
PATH_CLASSIFIER = 'classifiers/cascade-more-neg.xml'
PATH_COG        = 'templates/xcog-uncut.png'
PATH_COG_MASK   = 'templates/xcog-cut.png'
HIST_THRESHOLD  = 10        # cog detection, higher is more accurate for intersection
USE_LURE        = False
MAX_TIME_FISH   = 5         # minutes
# =======   End  =======

# initialize the WindowCapture class
moncap = MonitorCapture(MONITOR)

# load trained model, and cog template
detector = Detection(PATH_CLASSIFIER, PATH_COG, moncap.x_offset, HIST_THRESHOLD)

# load an empty Vision class
vision_bobber = Vision()

# load keyboard and mouse
joystick = Controller()

# Initialize KingFisher and give them a BrandName™ Joystick
kfb = KingFisherBot(joystick, moncap.x_offset, (moncap.mon_w, moncap.mon_h))

loop_time = time()
moncap.start()
detector.start()
kfb.start()

while(True):

    # make sure we have a screenshot first
    if moncap.screenshot is None:
        continue

    # Object detection
    detector.update(moncap.screenshot, joystick.mouse.position)

    # Draw MultiScale detection results
    detection_image = vision_bobber.draw_rectangles(moncap.screenshot, detector.rectangles)

    # display the images
    if DEBUG_DETECT:
        cv.imshow('Matches', detection_image)
    if DEBUG_FPS:
        print('FPS {}'.format(1 / (time() - loop_time)))
        loop_time = time()
    
    if kfb.state == BotState.INITIALIZING:
        # init requires no information
        pass
    elif kfb.state == BotState.CASTING:
        # casting needs no information
        pass
    elif kfb.state == BotState.SEARCHING:
        # give latest frames and locations for detected bobbers
        targets = vision_bobber.get_click_points(detector.rectangles)
        kfb.update_cog_status(detector.cog_confirmed)
        kfb.update_targets(targets)
        kfb.update_screenshot(moncap.screenshot)
        # give mouse cursor status
    elif kfb.state == BotState.FISHING:
        # active bobber found
        kfb.update_screenshot(moncap.screenshot)
        kfb.update_bobber_status(detector.fish_hooked)
    
    # waits 1 ms every loop to process key presses
    key = cv.waitKey(1)
    if key == ord('q'):
        kfb.stop()
        detector.stop()
        moncap.stop()
        cv.destroyAllWindows()
        break
    #elif key == ord('f'):
    #    cv.imwrite('positive/{}.jpg'.format(loop_time), screenshot)
    #elif key == ord('d'):
    #    cv.imwrite('negative/{}.jpg'.format(loop_time), screenshot)

print('Done.')
