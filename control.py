import time
import random

from pynput.mouse import Button, Controller as Mouse
from pynput.keyboard import Key, Controller as Keyboard

# Plundered from https://github.com/carpntr/harold

class Controller:
    def __init__(self):
        self.mouse = Mouse()
        self.keyboard = Keyboard()

    def move_mouse(self, x, y):
        def set_mouse_position(x, y):
            self.mouse.position = (int(x), int(y))

        def smooth_move_mouse(from_x, from_y, to_x, to_y, speed=0.2):
            steps = 40
            sleep_per_step = speed // steps
            x_delta = (to_x - from_x) / steps
            y_delta = (to_y - from_y) / steps
            for step in range(steps):
                new_x = x_delta * (step + 1) + from_x
                new_y = y_delta * (step + 1) + from_y
                set_mouse_position(new_x, new_y)
                time.sleep(sleep_per_step)

        return smooth_move_mouse(
            self.mouse.position[0],
            self.mouse.position[1],
            x,
            y
        )

    def left_mouse_click(self):
        self.mouse.click(Button.left)

    def right_mouse_click(self):
        self.mouse.click(Button.right)

    def shift_right_click(self):
        with self.keyboard.pressed(Key.shift_l):
            time.sleep(random.uniform(.2,.3)) # human reaction time
            self.right_mouse_click()
            time.sleep(1)

    def shift_left_click(self):
        with self.keyboard.pressed(Key.shift_l):
            time.sleep(random.uniform(.2,.3)) # human reaction time
            self.left_mouse_click()
            time.sleep(0.1)

    def jump(self):
        self.keyboard.press(Key.space)
        time.sleep(0.3)
        self.keyboard.release(Key.space)

    def cast(self):
        self.keyboard.press('=')
        time.sleep(0.2)
        self.keyboard.release('=')
        time.sleep(0.5)

    def lure_macro(self):
        self.keyboard.press('0')
        time.sleep(0.2)
        self.keyboard.release('0')
        time.sleep(random.uniform(5,6))

    def stand(self):
        self.keyboard.press('0')
        time.sleep(0.2)
        self.keyboard.release('0')
        time.sleep(0.5)

# if __name__ == '__main__':
#     time.sleep(2)
#     c = Controller()
#     c.shift_left_click()
