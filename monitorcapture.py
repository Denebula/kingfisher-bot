import numpy as np
import mss
from threading import Lock
from spool import Spool

class MonitorCapture(Spool):

    # threading properties
    stopped     = True
    lock        = None
    screenshot  = None
    # properties
    monitor     = None
    sct         = None
    mon_w       = 0
    mon_h       = 0
    x_offset    = 0

    
    def __init__(self, monitor_number):
        self.lock = Lock()
        self.sct = mss.mss()
        total_screen = self.sct.monitors[0]
        self.monitor = self.sct.monitors[monitor_number]
        self.mon_w = self.monitor["width"]
        self.mon_h = self.monitor["height"]
        self.mon_left = self.monitor["left"]
        # mouse position returns x,y across entire screenspace
        # use x_offset to get the x,y position on the screenshot
        if self.mon_left > 0:
            self.x_offset = total_screen["width"] - self.mon_left


    def get_screenshot(self):          
        img = np.array(self.sct.grab(self.monitor))
        return img


    def run(self):
        while not self.stopped:
            screenshot = self.get_screenshot()
            self.lock.acquire()
            self.screenshot = screenshot
            self.lock.release()

