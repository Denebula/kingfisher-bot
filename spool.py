from threading import Thread

class Spool:

    def start(self):
        self.stopped = False
        t = Thread(target=self.run)
        t.start()


    def stop(self):
        self.stopped = True
